export const envBaseUrl: string | undefined =
  process.env.NEXT_PUBLIC_BASE_API_URL;
export const apiResource: string = "api/v1/wallet";
