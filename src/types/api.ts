type ApiKey = string | undefined;
type ApiBaseUrl = string | undefined;

export type BaseApiConstructorTuple = [ApiKey, ApiBaseUrl];
