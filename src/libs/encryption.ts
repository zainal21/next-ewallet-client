import * as Base64 from "base-64";
import * as CryptoJS from "crypto-js";

export function generateJwtSignature(secretKey: string, payload: any) {
  const header = { typ: "JWT", alg: "HS256" };
  const stringHeader = JSON.stringify(header);
  const stringPayload = JSON.stringify(payload);

  const base64UrlHeader = btoa(stringHeader)
    .replace(/\+/g, "-")
    .replace(/\//g, "_")
    .replace(/=+$/, "");
  const base64UrlPayload = btoa(stringPayload)
    .replace(/\+/g, "-")
    .replace(/\//g, "_")
    .replace(/=+$/, "");

  const signature = CryptoJS.HmacSHA256(
    base64UrlHeader + "." + base64UrlPayload,
    secretKey
  );
  const base64UrlSignature = CryptoJS.enc.Base64.stringify(signature)
    .replace(/\+/g, "-")
    .replace(/\//g, "_")
    .replace(/=+$/, "");

  const jwt =
    base64UrlHeader + "." + base64UrlPayload + "." + base64UrlSignature;

  return jwt;
}
