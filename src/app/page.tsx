"use client";

import {
  Flex,
  Box,
  FormControl,
  FormLabel,
  Input,
  Stack,
  Button,
  Heading,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import * as Yup from "yup";
import Swal from "sweetalert2";
import { ErrorMessage, Field, Form, Formik, FormikHelpers } from "formik";
import { apiResource, envBaseUrl } from "@/constants/api";
import { generateJwtSignature } from "@/libs/encryption";
import { useRouter } from "next/navigation";

interface FormValues {
  email: string;
  password: string;
}

const validationSchema = Yup.object({
  email: Yup.string().email("Invalid email address").required("Email Required"),
  password: Yup.string().required("Password Required"),
});

interface Credentials {
  email: string;
  password: string;
}

interface UserData {
  id: string;
  email: string;
  name: string;
  phone_number: string | null;
  token: string;
  created_at: string | null;
  updated_at: string | null;
}

interface AuthResponse {
  status: string;
  timestamp: string;
  message: string;
  data: UserData;
  errors?: { [fieldName: string]: string[] };
}

const authenticateUser = async (
  credentials: Credentials,
  setFieldError: (field: string, message: string) => void
) => {
  const formData = new URLSearchParams();
  formData.append("email", credentials.email);
  formData.append("password", credentials.password);
  const formEncodedBody = formData.toString();

  const response = await fetch(`${envBaseUrl}/${apiResource}/auth/login`, {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      "X-Signature": generateJwtSignature("asololejos", credentials),
    },
    body: formEncodedBody,
  });

  if (!response.ok) {
    const errorResponse: AuthResponse = await response.json();
    const errorMessage = errorResponse?.message || "Authentication failed";
    if (errorResponse?.errors) {
      Object.entries(errorResponse.errors).forEach(([fieldName, errors]) => {
        setFieldError(fieldName, errors[0]); // Set the first error for each field
      });
    }

    throw new Error(errorMessage);
  }

  return response.json();
};

export default function Home() {
  const router = useRouter();

  const initialValues: FormValues = {
    email: "",
    password: "",
  };

  const onSubmit = async (
    values: FormValues,
    { setSubmitting, setFieldError }: FormikHelpers<FormValues>
  ) => {
    try {
      const response = await authenticateUser(values, setFieldError);
      const { data, message } = response;
      const sessionData = [
        { key: "token", value: data.token },
        { key: "email", value: data.email },
        { key: "name", value: data.name },
      ];
      sessionData.forEach(({ key, value }) =>
        sessionStorage.setItem(key, value)
      );
      Swal.fire({
        icon: "success",
        title: message,
      });
      router.push("/dashboard");
    } catch (error: any) {
      Swal.fire({
        icon: "error",
        title: error,
      });
    }
    setSubmitting(false);
  };

  return (
    <Flex
      minH={"100vh"}
      align={"center"}
      justify={"center"}
      bg={useColorModeValue("gray.50", "gray.800")}
    >
      <Stack spacing={8} mx={"auto"} maxW={"lg"} py={12} px={6}>
        <Stack align={"center"}>
          <Heading fontSize={"4xl"}>Sign in to your account</Heading>
        </Stack>
        <Box
          rounded={"lg"}
          bg={useColorModeValue("white", "gray.700")}
          boxShadow={"lg"}
          p={8}
        >
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={onSubmit}
          >
            {({ isSubmitting, isValid }) => (
              <Form>
                <Stack spacing={4}>
                  <FormControl id="email">
                    <FormLabel>Email address</FormLabel>
                    <Field type="text" as={Input} name="email" />
                    <ErrorMessage name="email">
                      {(msg) => <Text color="red">{msg}</Text>}
                    </ErrorMessage>
                  </FormControl>
                  <FormControl id="password">
                    <FormLabel>Password</FormLabel>
                    <Field type="password" as={Input} name="password" />
                    <ErrorMessage name="password">
                      {(msg) => <Text color="red">{msg}</Text>}
                    </ErrorMessage>
                  </FormControl>
                  <Stack spacing={10}>
                    <Button
                      type="submit"
                      bg={"blue.400"}
                      color={"white"}
                      _hover={{
                        bg: "blue.500",
                      }}
                      isDisabled={isSubmitting || !isValid}
                    >
                      Sign in
                    </Button>
                  </Stack>
                </Stack>
              </Form>
            )}
          </Formik>
        </Box>
      </Stack>
    </Flex>
  );
}
