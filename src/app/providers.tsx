"use client";

import { ChakraProvider } from "@chakra-ui/react";
import { ReactQueryProvider } from "./react-query-provider";

export function Providers({ children }: { children: React.ReactNode }) {
  return (
    <ReactQueryProvider>
      <ChakraProvider>{children}</ChakraProvider>
    </ReactQueryProvider>
  );
}
